#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "GFXFont.h"
#include "NotoSans.h"

int main()
{
    uint8_t buf[320*320] = {0};
    memset(buf, 0, 320*320);
    DrawGFXFont(&NotoSansMonoFont, buf, 320, 320, 1,1,"Hello", 5);
    FILE *fp = fopen("out_320x320.yuv", "wb");
    fwrite(buf, 1, 320*320, fp);
    fclose(fp);
    return 0;
}