#include "GFXFont.h"
#include "NotoSans.h"

void DrawGFXFont(const GFXfont *font, uint8_t *buf, int w, int h, int xpos, int ypos, char *str, int nchar)
{
    int curOffset = 0;
    for (int c = 0; c < nchar; c++) {
        char cc = str[c];
        if (cc < font->first || cc > font->last) {
            cc = font->first;
        }
        int charidx = str[c] - font->first;
        int bitmapbegin = font->glyph[charidx].bitmapOffset;
        int fontw = font->glyph[charidx].width;
        int fonth = font->glyph[charidx].height;
        for (int y = 0; y < fonth; y++) {
            for (int x = 0; x < fontw; x++) {
                int bitIndex = x + fontw * y;         // Bit index in array of bits
                int byte = bitIndex >> 3;             // Byte offset of bit, assume 8 bit bytes
                int bitMask = 0x80 >> (bitIndex & 7); // Individual bit in byte array for bit
                int byteIndex = bitmapbegin + byte;
                if (font->bitmap[byteIndex] & bitMask) {
                    int targetpos = (ypos + font->yAdvance + y + font->glyph[charidx].yOffset) * w + (xpos + curOffset + x + font->glyph[charidx].xOffset);
                    buf[targetpos] = 255;
                }
            }
        }
        curOffset += font->glyph[charidx].xAdvance;
    }
}