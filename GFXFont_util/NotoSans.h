#ifndef _NOTOSANS_H_
#define _NOTOSANS_H_

#include "GFXFont.h"

extern const uint8_t NotoSansMono_Medium28pt7bBitmaps[];
extern const GFXglyph NotoSansMono_Medium28pt7bGlyphs[];
extern const GFXfont NotoSansMonoFont;

#endif