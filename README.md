# Utilities collection

This repo collects some useful utilities written by me, and used frequently.

- CRC checksum  
  Used in UART communication between PC and MCU.
- int2str  
  Used in some MCU which not implemented itoa function.
- GFXFont
  Demo code to draw text on raw image buffer, can't used directly, these code just for reference.
